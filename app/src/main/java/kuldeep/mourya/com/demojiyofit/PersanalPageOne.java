package kuldeep.mourya.com.demojiyofit;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.wx.wheelview.widget.WheelView;

/**
 * Created by kulde on 10/29/2016.
 */

public class PersanalPageOne extends Fragment {

    WheelView wheelView;
    Button button;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root= inflater.inflate(R.layout.fragment_persanal_page_one,container,false);
        wheelView=(WheelView)root.findViewById(R.id.wheelview);
        button=(Button)root.findViewById(R.id.button);

        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id="
                                + "com.mourya.kuldeep.cgpitopercentage"));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //the device hasn't installed Google Play
                    Toast.makeText(getActivity(), "You don't have Google Play installed", Toast.LENGTH_LONG).show();

                }
            }
        });
    }
}
