package kuldeep.mourya.com.demojiyofit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by kulde on 10/29/2016.
 */

public class S {



    /****************Common*******************/
    public static final String TAG = "CCC";
    public static final String BLANK = "";
    public static final String SP_USER_INFO = "UserInfo";
    public static final String SP_LOGIN_INFO = "LoginInfo";
    public static final String SP_WORKOUT = "Workout";
    public static final String DOT = ".";

    /*************MainActivity****************/
    public static final int REQUEST_OAUTH = 1001;
    public static final String AUTH_PENDING = "isAuthPending";
    public static final String LI_AUTHENTICATED = "authenticated";
    public static final String RECORDAPI_SUBSCRIBED = "subscribed";
    public static final long SPLASH_TIME_OUT = 1000;
    public static final String UI_GENDER_AGE = "genderAgeUpdated";
    public static final String UI_HEIGHT_WEIGHT = "heightWeightUpdated";
    public static final String SPLASH_SCREEN = "splashScreen";
    public static final String AUDIO_NOTIFICATION_PATH = "audioNotificationPath";

    /****************DBAdapter*********************/
    public static final String FOOD_TABLE = "Food_Table";
    public static final String FOOD_ID = "id";
    public static final String FOOD_NAME = "food";
    public static final String TYPE = "type";
    public static final String HINDI_NAME = "hindi_name";
    public static final String IMP_FOOD = "is_important_food";
    public static final String MEAL_TIMES = "meal_times";
    public static final String OTHER_MEAL_TIME = "other_meal_time_possibility";
    public static final String SUBDISHES = "subdishes";
    public static final String CALORIES = "calories";
    public static final String UNIT = "unit";
    public static final String PROTEIN = "protein";
    public static final String CARBS = "carbohydrates";
    public static final String FAT = "fat";
    public static final String FIBRE = "fibre";


    /***************DietActivity******************/
    public static final String HAS_SUBDISHES = "has_subdishes";
    public static final String NONE = "none";
    public static final String ENERGY = "energy";
    public static final String NUTRITION = "nutrition";
    public static final String COMMA = ",";
    public static final String CLICKED = "clicked";
    public static final String UNCLICKED = "unclicked";
    public static final String BASE_ITEM = "base_item";

    /***************WorkoutActivity***************/
    public static final String W_NEW_DAY_CODE = "newDayCode";
    public static final int STORAGE_PERMISSION_CODE = 200;
    public static final String SP_SETTINGS = "Settings";
    public static final Integer EXERCISE_INTRO_DURATION = 10;

    /*****************SubdishAdapter****************/
    public static final String FOOD = "food";
    public static final String RESID = "food_rb_id";

    /*************NotificationService****************/
    public static final int AUDIO_NOTIFICATION_ID = 1001;
    public static final String KEY_NOTIFICATION = "notificationID";

    /***************WaterAdapter********************/
    public static final String W_LAST_HYDRATION_UPDATE_TIME = "lastHydrationUpdateTime";
    public static final String W_LAST_UPDATED_HYDRATION = "lastUpdatedHydration";

    /*****************ArticleFragment****************/
    public static final int RADIOGROUP_ID = 5000;
    public static final int CHECKBOX_ID = 5500;
    public static final int DATA_FETCH_PROGRESSBAR = 9000;
    public static final int FEED_TYPE_IMAGE_QUIZ = 9001;
    public static final int FEED_TYPE_ARTICLE = 9002;
    public static final int FEED_TYPE_QUIZ = 9003;
    public static final String TAG_METADATA = "_metadata";
    public static final String LINKS = "Links";
    public static final String NEXT_URL = "next";
    public static final String FEED_ARRAY = "feed";
    public static final String KEY_ARTICLE_PARCELABLE = "articleParcelable";
    public static final String KEY_QUIZ_PARCELABLE = "quizParcelable";
    public static final String KEY_QUESTION_NUM = "questionNum";
    public static final int OPTION_IB_COUNT = 4;
    public static final String OPTION_IB_TAG = "ibOption";
    public static final String NA = "NA";

    /*****************FeedbackFragment****************/
    public static final String LANGUAGE_ENGLISH = "en";
    public static final String UI_KEYBOARDLANG = "keyboardLang";
    public static final String UI_PHONE_NUMBER = "phoneNumber";
    public static final String ITEM_IMAGE_URL = "base_item_image";
    public static final String ITEM_SUBDISHES = "subdishes_with_item";
    public static final String S_ROOT_DIR = "rootDir";
    public static final String S_AUDIO_DIR = "audioDir";
    public static final String S_GIF_DIR = "gifDir";
    public static final String GIF = "gif";
    public static final String GIF_EXTENSION = ".gif";
    public static final String AUDIO_EXTENSION = ".amr";
    public static final String UNDERSCORE = "_";
    public static final String AUDIO_SILENCE = "silence.mp3";
    public static final String AUDIO_SWITCH_SIDES = "cha";

    public interface ACTION {
        String MAIN = "in.jiyofit.gfit.action.main";
        String STOP = "in.jiyofit.gfit.action.stop";
        String PLAY = "in.jiyofit.gfit.action.play";
        String PAUSE = "in.jiyofit.gfit.action.next";
        String REPLAY = "in.jiyofit.gfit.action.replay";
        String START_FOREGROUND = "in.jiyofit.gfit.action.startforeground";
        String STOP_FOREGROUND = "in.jiyofit.gfit.action.stopforeground";
    }

    public interface WorkoutKeys{
        String WORKOUT = "workout";
        String DAY_CODE = "day_code";
        String ORDER_NUM = "order_num";
        String EXERCISE_INFO = "exercise_info";
        String EXERCISE_ID = "exercise_id";
        String EXERCISE_NAME = "exercise_name";
        String EXERCISE_URL = "exercise_url";
        String DISPLAY_NAME = "display_name";
        String PARAMETERS = "parameters";
        String TARGET = "target";
        String AUDIO = "audio";
        String AUDIO_PRE = "audio_pre";
        String AUDIO1 = "audio1";
        String AUDIO2 = "audio2";
        String AUDIO3 = "audio3";
        String AUDIO_FILE = "audio_file";
        String AUDIO_URL = "audio_url";
        String RECOMMENDED_REPS = "recommended_reps";
        String RECOMMENDED_WEIGHT = "recommended_weight";
        String DURATION = "duration";
        String WORKOUT_DATE = "workout_date";
        String WORKOUT_CODE = "workout_code";
    }

    public interface FeedKeys {
        String FEED_ID = "feed_id";
        String FEED_TYPE_CODE = "feed_type_code";
        String FEED_JSON = "feed_json";
        String IMAGE_QUIZ_ID = "image_quiz_id";
        String QUESTION = "question";
        String OPTION_COUNT = "option_count";
        String OPTION_NUM = "option_num";
        String OPTIONS = "options";
        String OPTION_IMAGE = "option_image_url";
        String OPTION_NAME = "option_name";
        String COUNT = "answer_count";
        String ANSWER = "correct_answer";
        String ARTICLE_ID = "article_id";
        String ARTICLE_TITLE = "article_title";
        String ARTICLE_IMAGE = "article_image";
        String ARTICLE_TEXT = "article_text";
        String QUIZ_ID = "quiz_id";
        String QUESTION_COUNT = "question_count";
        String QUESTIONS = "questions";
        String QUESTION_ID = "question_id";
        String QUESTION_TYPE = "question_type";
        String CORRECT_ANSWER = "correct_answer";
        String SINGLE_OPTION = "single_answer";
        String MULTI_ANSWER = "multi_answer";
    }

   // public static final Typeface engFont = AppApplication.AppEngFont;
    //public static final Typeface hindiFont = AppApplication.AppHindiFont;
   // public static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

    /*************DailyHydrationAsyc****************/
    public static final String WATER_INTAKE = "waterIntakeLiters";
    public interface WaterUnit {
        String GLASS = "glass";
        int GLASS_ML = 250;
        String CUP = "cup";
        int CUP_ML = 150;
        String HALF_LITRE = "halfLitre";
        int HALF_LITRE_ML = 500;
        String LITRE_BOTTLE = "litre";
        int LITRE_BOTTLE_ML = 1000;
        String TETRAPACK = "tetrapack";
        int TETRAPACK_ML = 160;
        String COLD_DRINK = "coldDrink";
        int COLD_DRINK_ML = 200;
    }

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void L(Object s){
        Log.d(TAG, s.toString());
    }

    public static int dpToPx(Context context, int dp){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int)((dp * displayMetrics.density) + 0.5);
    }

    public static float spToPx(Context context, float sp){
        final float scale = context.getResources().getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static void toastIt(Context ctx, int toastTextID) {
        String message = ctx.getString(toastTextID);
        Toast toast = Toast.makeText(ctx, message, Toast.LENGTH_LONG);
        TextView textView = new TextView(ctx);
        textView.setBackgroundColor(Color.DKGRAY);
        textView.setTextColor(Color.WHITE);
       // textView.setTypeface(hindiFont);
        textView.setTextSize(22);
        textView.setText(message);
        textView.setPadding(30, 20, 30, 20);
        toast.setView(textView);
        toast.show();
    }

    public interface Parameters {
        String  INTRODUCTION = "introduction";
        String  FEEDBACK = "feedback";
        String  SWITCH_SIDES = "switch_sides";
        String  FIXED_DURATION = "fixed_duration";
        String  REPS = "reps";
        String  WEIGHT = "weight";
        String  REP_TIME_CONTROL = "rep_time_control";
    }

    public interface Target {
        String  PRIMARY_TARGET = "primary_target";
        String  SECONDARY_TARGET = "secondary_target";
    }


}
